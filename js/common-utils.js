var PORTRAIT_TYPE = 'portrait';
var SHIP_TYPE = 'ship';
var CHILD_TYPE = 'child';

function getImageFromURL() {
	var url_string = window.location.href;
	var url = new URL(url_string);
	//Second attribute tells it to return just the image without any formatting
	var c = url.searchParams.get("imageLink") + '&custom_att_3=NLA';
	return c;
}

function getParameterFromURL(paramName){
	var url_string = window.location.href;
	var url = new URL(url_string);
	var c = url.searchParams.get(paramName);
	return c;
}