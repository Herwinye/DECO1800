if (location.protocol != 'https:')
{
 location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}
import QrScanner from './qr-scanner.min.js';
QrScanner.WORKER_PATH = 'js/qr-scanner-worker.min.js';
QrScanner.DEFAULT_CANVAS_SIZE = 400;
var videoElem = document.getElementById('video')
const qrScanner = new QrScanner(videoElem, HandleResult);
qrScanner.setInversionMode("both");
qrScanner.start();

var expectedQRCodeType;

function HandleResult(result){
    var parts = result.split(';')
    var type = parts[0];
    var url = parts[1];
    if(type != expectedQRCodeType)
    {
        $('#QRbox').css('border', 'solid red 4px');
        return;
    }
    var gameToPlay;
    switch(type){
        case SHIP_TYPE:
            gameToPlay = 'puzzle.html';
            break;
        case PORTRAIT_TYPE:
            gameToPlay = 'quiz.html';
            break;
        case CHILD_TYPE:
            gameToPlay = 'seekGame.html';
            break;
        default:
            return;
    }
    window.location.href = (gameToPlay + '?imageLink='+ url);
}

$(document).ready(function () {
    expectedQRCodeType = getParameterFromURL('type');
});

