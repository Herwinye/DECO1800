var exactLink;
var currentWorth = 50;

function loadInitialImage(data) {
	var id = data.result.records[0]["_id"]

	var new_data = {
		sql: 'SELECT * from "8a327cf9-cff9-4b34-a461-6f883bdc3a48" WHERE "_id" >= ' + id + ' AND "_id" <=' + (id + 5)
	}
	$.ajax({
		url: 'https://www.data.qld.gov.au/api/3/action/datastore_search_sql',
		data: new_data,
		dataType: 'jsonp',
		cache: true,
		success: function (new_data) {
			loadTheRestOfImages(new_data);
		}
	});

}

function loadTheRestOfImages(data) {
	records = data.result.records
	cards = [];
	for (i = 0; i < records.length - 2; i++) {
		cards[i] = {
			name: "",
			img: records[i]["1000_pixel_jpg"],
			id: records[i]["_id"],
		}
	}
	initializeGame(cards);
}

$(document).ready(function () {
	imageSource = getImageFromURL();
	var data = {
		sql: 'SELECT * from "8a327cf9-cff9-4b34-a461-6f883bdc3a48" WHERE "1000_pixel_jpg" LIKE' + "'" + imageSource + "'"
	}

	$.ajax({
		url: 'https://www.data.qld.gov.au/api/3/action/datastore_search_sql',
		data: data,
		dataType: 'jsonp',
		cache: true,
		success: function (data) {
			loadInitialImage(data);
		}
	});
	exactLink = getImageFromURL();
	$('#BackButton').click(function(){
		window.location.href = "qrcode.html?type="+CHILD_TYPE;
	});
});

function initializeGame(cards) {
	var Memory = {

		init: function (cards) {
			this.$game = $(".game");
			this.$modal = $(".modal");
			this.$overlay = $(".modal-overlay");
			this.$restartButton = $("button.restart");
			this.cardsArray = $.merge(cards, cards);
			this.shuffleCards(this.cardsArray);
			this.setup();
		},

		shuffleCards: function (cardsArray) {
			this.$cards = $(this.shuffle(this.cardsArray));
		},

		setup: function () {
			this.html = this.buildHTML();
			this.$game.html(this.html);
			this.$memoryCards = $(".card");
			this.paused = false;
			this.guess = null;
			this.binding();
		},

		binding: function () {
			this.$memoryCards.on("click", this.cardClicked);
			this.$restartButton.on("click", $.proxy(this.reset, this));
		},
		// kinda messy but hey
		cardClicked: function () {
			var _ = Memory;
			var $card = $(this);
			if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {
				$card.find(".inside").addClass("picked");
				if (!_.guess) {
					_.guess = $(this).attr("data-id");
				} else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
					$(".picked").addClass("matched");
					_.guess = null;
					currentWorth += 5;
				} else {
					_.guess = null;
					_.paused = true;
					if(currentWorth > 0)
						currentWorth -= 5;
					setTimeout(function () {
						$(".picked").removeClass("picked");
						Memory.paused = false;
					}, 600);
				}
				if ($(".matched").length == $(".card").length) {
					_.win();
				}
			}
		},

		win: function () {
			this.paused = true;
			add_to_score(currentWorth, CHILD_TYPE);
			setTimeout(function () {
				Memory.showModal();
				Memory.$game.fadeOut();
			}, 1000);
			setTimeout(function(){
				window.location.href = "details.html?type=" + CHILD_TYPE +"&imageLink=" + exactLink;
			}, 3000);
		},

		showModal: function () {
			this.$overlay.show();
			this.$modal.fadeIn("slow");
		},

		hideModal: function () {
			this.$overlay.hide();
			this.$modal.hide();
		},

		reset: function () {
			this.hideModal();
			this.shuffleCards(this.cardsArray);
			this.setup();
			this.$game.show("slow");
		},

		// Fisher--Yates Algorithm -- https://bost.ocks.org/mike/shuffle/
		shuffle: function (array) {
			var counter = array.length, temp, index;
			// While there are elements in the array
			while (counter > 0) {
				// Pick a random index
				index = Math.floor(Math.random() * counter);
				// Decrease counter by 1
				counter--;
				// And swap the last element with it
				temp = array[counter];
				array[counter] = array[index];
				array[index] = temp;
			}
			return array;
		},

		buildHTML: function () {
			var frag = '';
			this.$cards.each(function (k, v) {
				frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
				<div class="front"><img src="'+ v.img + '"\
				alt="'+ v.name + '" /></div>\
				<div class="back"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/74196/codepen-logo.png"\
				alt="Codepen" /></div></div>\
				</div>';
			});
			return frag;
		}
	};
	Memory.init(cards);
}

