function initMap(location, popup_text){
	var myMap = L.map("map").setView([-21, 148], 4);

	L.tileLayer("https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw", {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' + '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' + 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: "mapbox.streets"
	}).addTo(myMap);

	var latLong = location.split(";");
		if (latLong.length > 1) {
			var latValue = parseInt(latLong[1].split(",")[0].trim());
			var lngValue = parseInt(latLong[1].split(",")[1]);
			// Position the marker and add to map
			var marker = L.marker([latValue, lngValue]).addTo(myMap);

			// Associate a popup with the record's information
			popupText = "<strong>" + popup_text + "</strong>";
			marker.bindPopup(popupText).openPopup();
	}
}

function processRecord(data) {

	var recordValue = data.result.records[0];
	//get img link
	var recordImageLarge = recordValue["1000_pixel_jpg"];
	var recordDescription = recordValue["dc:description"];
	var recordsubject = recordValue["dc:subject"];
	var recordTitle = recordValue["dc:title"];
	var recordpublisher = recordValue["dc:publisher"];
	var recordYear = recordValue["dcterms:temporal"];
	var recordLocation = recordValue["dcterms:spatial"];
	var recordID = recordValue["_id"];

	initMap(recordLocation,recordTitle);

	var recordTemplate = $(".record-template");
	recordTemplate.attr("id", "record-" + recordID).removeClass("record-template");
	recordTemplate.appendTo("#records");

	$("#record-" + recordID + " .title").html(recordTitle);
	$("#record-" + recordID + " .description").html(recordDescription);
	$("#record-" + recordID + " img").attr("src", recordImageLarge);
	$("#record-" + recordID + " .record-content a").attr("href", recordImageLarge);
	$("#record-" + recordID + " .subject").html(recordsubject);
	$("#record-" + recordID + " .publisher").html(recordpublisher);
	$("#record-" + recordID + " .year").html(recordYear);
}

$(document).ready(function () {
	imageSource = getImageFromURL();
	used_type = getParameterFromURL('type');
	var data = {
		sql: 'SELECT * from "8a327cf9-cff9-4b34-a461-6f883bdc3a48" WHERE "1000_pixel_jpg" LIKE' + "'" + imageSource + "'"
	}

	$.ajax({
		url: "https://www.data.qld.gov.au/api/3/action/datastore_search_sql",
		data: data,
		dataType: "jsonp",
		cache: true,
		success: function (result) {
			processRecord(result);
		}
	})

	$('#BackButton').click(function(){
		window.location.href = 'qrcode.html?type=' + used_type;
	});
}); 