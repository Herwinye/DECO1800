
//This function must be called from one of the user input handlers, othewise it will be ignored by the browser
function requestFullScreen(element) {
    // Supports most browsers and their versions.
    if (element.requestFullscreen) {
        element.requestFullscreen();
      } else if (element.mozRequestFullScreen) { /* Firefox */
        element.mozRequestFullScreen();
      } else if (element.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        element.webkitRequestFullscreen();
      } else if (element.msRequestFullscreen) { /* IE/Edge */
        element.msRequestFullscreen();
      }
    //Well, if we can't get it to work full-screen - too bad, not a compulsory requirement
}