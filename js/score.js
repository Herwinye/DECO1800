function init_score(type){
    localStorage.setItem('score_' + type , 0);
}

function get_score(type){
    var score = localStorage.getItem('score_' + type, 0);
    return score;
}

function set_score(score, type){
    if(Number.isInteger(score)){
        localStorage.setItem('score_' + type, score);
    }
}

function add_to_score(amount, type){
    if(!Number.isInteger(amount) || amount < 0){
        return;
    }
    score = get_score(type);
    if(!score){
        init_score(type);
        set_score(amount, type);
    }
    else{
        set_score(amount + score, type);
    }
}