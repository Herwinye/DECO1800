//Scene elements required to work publically
var cube, camera, scene, render, orientationControls;
//Constants to identify sides of a cube by material index
var RIGHT_SIDE_INDEX = 0, LEFT_SIDE_INDEX = 1, TOP_INDEX = 2, BOTTOM_INDEX = 3, FRONT_INDEX = 4, BACK_INDEX = 5;
//Functions to execute after touching cubes side
var callbacks_dictionary;

//General handler to update the view after turning the camera and changing visuals
function animate() {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
}

//Handles resize of the screen - changes the rendering area size and adjusts
//camera aspect ratio
function onWindowResize() {
    controls.update();
    camera.aspect = window.innerWidth / window.innerHeight;
    renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
    renderer.setSize(window.innerWidth, window.innerHeight);
    //Applies camera settings
    camera.updateProjectionMatrix();
    renderer.render(scene, camera);
}

//Device pixel ratio is different from the actual resolution so calculate it
function calcDevicePixelRatio() {
    var ctx = document.createElement("canvas").getContext("2d"),
        dpr = window.devicePixelRatio || 1,
        bsr = ctx.webkitBackingStorePixelRatio ||
            ctx.mozBackingStorePixelRatio ||
            ctx.msBackingStorePixelRatio ||
            ctx.oBackingStorePixelRatio ||
            ctx.backingStorePixelRatio || 1;

    return dpr / bsr;
}

//Try to guess the appropriate canvas size based on ratio
function createHiDPICanvas(w, h, ratio) {
    if (!ratio) { ratio = calcDevicePixelRatio(); }
    var can = document.createElement("canvas");
    can.width = w * ratio;
    can.height = h * ratio;
    can.style.width = w + "px";
    can.style.height = h + "px";
    can.getContext("2d").setTransform(ratio, 0, 0, ratio, 0, 0);
    return can;
}

//Creates a text canvas material(!) with given parameters
function createTextCanvas(text, font, foreground, background, xres, yres, backOpacity) {
    var canvas = createHiDPICanvas(xres, yres);
    var context = canvas.getContext("2d");
    context.globalAlpha = backOpacity;
    context.font = font;
    context.textAlign = "center";
    context.fillStyle = background;
    context.fillRect(0, 0, xres, yres);
    context.fillStyle = foreground;
    context.fillText(text, xres / 2, yres / 2);
    var texture = new THREE.Texture(canvas);
    texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
    texture.needsUpdate = true;
    var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true });
    return material;
}

function createPictureCanvas(img, showOnBothSides){       
    var texture = THREE.ImageUtils.loadTexture(img.src); 
    texture.anisotropy = renderer.capabilities.getMaxAnisotropy(); 
    texture.needsUpdate = true; 
    if(showOnBothSides) 
        var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true, side: THREE.DoubleSide }); 
    else 
        var material = new THREE.MeshBasicMaterial({ map: texture, transparent: true }); 
    return material; 
} 

//Casts a ray from the camera position and executes a callback if it hits one of the cube sides
function onDocumentMouseDown(event) {
    event.preventDefault();

    var raycaster = new THREE.Raycaster();
    var mouse = new THREE.Vector2();

    mouse.x = (event.clientX / renderer.domElement.clientWidth) * 2 - 1;
    mouse.y = - (event.clientY / renderer.domElement.clientHeight) * 2 + 1;

    raycaster.setFromCamera(mouse, camera);

    var intersects = raycaster.intersectObjects(scene.children);

    if (intersects.length > 0) {
        var sideIndex = intersects[0].face.materialIndex;
        callbacks_dictionary[sideIndex]();
    }

}

//Callbacks for cube's sides
function frontCallback(){
    
}

function backCallback(){
    
}

function leftCallback(){
    window.location.href = "qrcode.html?type=portrait";
}

function rightCallback(){
    window.location.href = "qrcode.html?type=ship";
}

function topCallback(){
    window.location.href = "qrcode.html?type=child";
}

function bottomCallback(){
    
}

//Initializes the scene with a cube, camera and controls to rotate it around
function init() {
    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 1, 100);

    renderer = new THREE.WebGLRenderer({ alpha: true });
    //There goes your background color
    renderer.setClearColor( 0xFFFFFF, 1);

    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    controls = new THREE.TrackballControls(camera);

    //We don't want it to move around, only rotate
    controls.noZoom = true;
    controls.noPan = true;

    //Speed of rotation, has much more effect on the tablet than on desktop, be careful
    controls.rotateSpeed = 2.0;

    //Is there a better cube size calc formula?
    cubeSize = window.innerWidth >= innerHeight ? window.innerHeight / 200 : window.innerWidth / 300;

    var geometry = new THREE.CubeGeometry(cubeSize, cubeSize, cubeSize);

    sidesBackground = 0x66FCF1;
    sidesOpacity = 0.8;

    //Just create an image
    var leftSideImage = new Image();
    var rightSideImage = new Image();
    var topSideImage = new Image();
    var bottomSideImage = new Image();
    var frontSideImage = new Image();
    var backSideImage = new Image();

    //Point it to one of your images in a folder, .. means go one directory back, so it goes back from
    // js folder and looks on the project root level
    leftSideImage.src = "../images/people4.png";
    rightSideImage.src = "../images/ship23.png";
    topSideImage.src = "../images/kids1.png"
    bottomSideImage.src = "../images/name.png"
    frontSideImage.src = "../images/destop.png"
    backSideImage.src = "../images/logo.png"

    //Call this thing with your image, second parameter decides whether or not you want it to show up on both
    //front and back side of an edge
    //Different materials let us know which side exactly we hit when clicking a mouse
    var leftSide = createPictureCanvas(leftSideImage, true);
    var rightSide = createPictureCanvas(rightSideImage, true);
    var topSide = createPictureCanvas(topSideImage, true);
    var bottomSide = createPictureCanvas(bottomSideImage, true);
    var frontSide = createPictureCanvas(frontSideImage, true);
    var backSide = createPictureCanvas(backSideImage, true);
    
    //Order DOES matter, right side first, then left, then top, then bottom, then front, then back
    var cubeMaterials = [rightSide, leftSide, topSide, bottomSide, frontSide, backSide];
    cube = new THREE.Mesh(geometry, cubeMaterials);

    // edges geometry
    var geometry = new THREE.EdgesGeometry( cube.geometry ); // or WireframeGeometry
    var material = new THREE.LineBasicMaterial( { color: 0x000000, linewidth: 1 } );
    var edges = new THREE.LineSegments( geometry, material );
    cube.add( edges ); // add wireframe as a child of the parent mesh

    scene.add(cube);

    //Move camera back a bit so we don't start inside the cube
    camera.position.x = 0;
    camera.position.y = 0;
    camera.position.z = 5;

    //Functions to call when we click sides of a cube
    callbacks_dictionary = 
    {
        [RIGHT_SIDE_INDEX]: rightCallback,
        [LEFT_SIDE_INDEX]: leftCallback, 
        [TOP_INDEX]: topCallback,
        [BOTTOM_INDEX]: bottomCallback,
        [FRONT_INDEX]: frontCallback,
        [BACK_INDEX]: backCallback
    };

    //Event handlers
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
}

init();
animate();