var exactLink;
var currentWorth = 10;

function getYear(year) {
	if (year) {
		return year.match(/[\d]{4}/);
	}
}

function quiz(results) {

	var decadeArray = ["1900s", "1910s", "1920s", "1930s", "1940s", "1950s", "1960s", "1970s", "1980s", "1990s", "2000s"];
	var buttonArray = ["#answer1", "#answer2", "#answer3"];
	
	var recordValue = results.result.records[0];
	var recordYear = getYear(recordValue["dcterms:temporal"]);		

	var recordImage = recordValue["1000_pixel_jpg"];
	var recordDecade = Math.floor(parseInt(recordYear, 10) / 10) * 10;

	$("#qld_picture").attr("src", recordImage);
	//randomly choose a correct button in three buttons
	var correctButton = buttonArray[Math.round(Math.random() * 2)];
	//get a random button and make it the correct answer
	$(correctButton).html(recordDecade + "s"); 

	for (var button of buttonArray) {
		if (button != correctButton) {
			//make the other two answer is different to the correct answer
			while(true){
				var decade = decadeArray[Math.round(Math.random() * 10)];
				if (decade != (recordDecade + "s")) {
					break;
				}
			}
			$(button).html(decade);
		}
	}

	$(buttonArray[0]).click(function(){
		if (buttonArray[0] === correctButton) {
			continueToDetail();
		}else {
			window.alert("Sorry, you are wrong");
			if(currentWorth > 0)
				currentWorth -= 5;
		}
	});
	
	$(buttonArray[1]).click(function(){
		if (buttonArray[1] === correctButton) {
			continueToDetail();
		}else {
			window.alert("Sorry, you are wrong");
			if(currentWorth > 0)
				currentWorth -= 5;
		}
	});

	$(buttonArray[2]).click(function(){
		if (buttonArray[2] === correctButton) {
			continueToDetail()
		}else {
			window.alert("Sorry, you are wrong");
			if(currentWorth > 0)
				currentWorth -= 5;
		}
	});
}

function continueToDetail(){
	add_to_score(currentWorth, PORTRAIT_TYPE);
	window.location.href = "details.html?type=" + PORTRAIT_TYPE + "&imageLink=" + exactLink;
}

$(document).ready(function () {
	var imageSource = getImageFromURL();
	exactLink = getImageFromURL();
	var data = {
        sql:'SELECT * from "8a327cf9-cff9-4b34-a461-6f883bdc3a48" WHERE "1000_pixel_jpg" LIKE' + "'" +imageSource+"'"
	}	

	$.ajax({
		url: 'https://www.data.qld.gov.au/api/3/action/datastore_search_sql',
		data: data,
		dataType: 'jsonp',
		cache: true,
		success: function (data) {
			quiz(data);
		}
	});
});