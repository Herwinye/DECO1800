var currentWorth = 100;

const PuzzleDimension = 3;
const puzzleHoverColor = '#66fcf1';

var puzzleCanvas;
var drawingSurface;

var exactLink;

var originalImage;
var pieces;
var puzzleWidth;
var puzzleHeight;
var pieceWidth;
var pieceHeight;
var selectedPiece;
var pieceToDrop;

var coordinates;
var touchMeDaddy;

var xScaling;
var yScaling;

//Determines whether on not user device supports touch screen
function checkTouchScreen() {
    if ("ontouchstart" in document.documentElement) {
        return true;
    }
    else {
        return false;
    }
}

function calcScaling(width, height, new_height){
    var ratio = height / width;
    var new_width = new_height / ratio;
    return new_width;
}



function init() {
    originalImage = new Image();
    originalImage.addEventListener('load', onImageLoad, false);
    exactLink = getImageFromURL();
    originalImage.src = getImageFromURL();
    touchMeDaddy = checkTouchScreen();
}

function onImageLoad(e) {
    var heightToFit = Math.min(window.innerHeight * 0.9, originalImage.height);
    var widthToFit = calcScaling(originalImage.width, originalImage.height, heightToFit);
    pieceWidth = Math.floor(widthToFit / PuzzleDimension);
    pieceHeight = Math.floor(heightToFit / PuzzleDimension);
    puzzleWidth = pieceWidth * PuzzleDimension;
    puzzleHeight = pieceHeight * PuzzleDimension;
    xScaling = (originalImage.width / PuzzleDimension / pieceWidth);
    yScaling = (originalImage.height / PuzzleDimension / pieceHeight);
    setCanvas();
    initPuzzle();
    createTitle("Click to Start Puzzle");
    buildPieces();
}

function setCanvas() {
    puzzleCanvas = document.getElementById('puzzleCanvas');
    drawingSurface = puzzleCanvas.getContext('2d');
    puzzleCanvas.width = puzzleWidth;
    puzzleCanvas.height = puzzleHeight;
}

function initPuzzle() {
    pieces = [];
    coordinates = { x: 0, y: 0 };
    selectedPiece = null;
    pieceToDrop = null;
    //drawingSurface.drawImage(originalImage, 0, 0, puzzleWidth, puzzleHeight, 0, 0, puzzleWidth, puzzleHeight);
    var width = calcScaling(originalImage.width, originalImage.height, Math.min(window.innerHeight * 0.9, originalImage.height))
    drawingSurface.drawImage(originalImage,0,0, width, Math.min(window.innerHeight, originalImage.height));
}

function createTitle(msg) {
    drawingSurface.fillStyle = "#000000";
    drawingSurface.globalAlpha = .4;
    drawingSurface.fillRect(100, puzzleHeight - 40, puzzleWidth - 200, 40);
    drawingSurface.fillStyle = "#FFFFFF";
    drawingSurface.globalAlpha = 1;
    drawingSurface.textAlign = "center";
    drawingSurface.textBaseline = "middle";
    drawingSurface.font = "20px Arial";
    drawingSurface.fillText(msg, puzzleWidth / 2, puzzleHeight - 20);
}

function buildPieces() {
    var piece;
    var xPos = 0;
    var yPos = 0;
    for (var i = 0; i < PuzzleDimension * PuzzleDimension; i++) {
        piece = {};
        piece.sx = xPos;
        piece.sy = yPos;
        pieces.push(piece);
        xPos += pieceWidth;
        if (xPos >= puzzleWidth) {
            xPos = 0
            yPos += pieceHeight;
        }
    }
    if(!touchMeDaddy)
        document.onmousedown = shufflePuzzle;
    else
        document.ontouchend = shufflePuzzle;
}

function shufflePuzzle() {
    pieces = shuffleArray(pieces);
    drawingSurface.clearRect(0, 0, puzzleWidth, puzzleHeight);
    var piece;
    var xPos = 0;
    var yPos = 0;
    for (var i = 0; i < pieces.length; i++) {
        piece = pieces[i];
        piece.xPos = xPos;
        piece.yPos = yPos;
        //x,y,x part to take,y part to take, new x position, new y position, size x to match, size y to match 
        drawingSurface.drawImage(
            originalImage, //image
             piece.sx * xScaling, piece.sy * yScaling, //x and y to aim for
              pieceWidth * xScaling,
               pieceHeight * yScaling , //x and y size on the initial image
               xPos, yPos, //x and y position on new image
                pieceWidth, pieceHeight); //x and y size to match
        //drawingSurface.strokeRect(xPos, yPos, pieceWidth, pieceHeight);
        xPos += pieceWidth;
        if (xPos >= puzzleWidth) {
            xPos = 0;
            yPos += pieceHeight;
        }
    }
    if(!touchMeDaddy)
        document.onmousedown = onPuzzleClick;
    else{
        document.ontouchend = null;
        document.ontouchstart = onPuzzleClick;
    }
}

function shuffleArray(o) {
    for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function onPuzzleClick(e) {
    //First two are likely to be hit in a browser using desktop\laptop and mouse
    //The third one handles touchscreen, in case of surface or other hybrid - the touch screen
    //input should override mouse
    if (e.layerX || e.layerX == 0) {
        coordinates.x = e.layerX; //+ puzzleCanvas.offsetLeft;
        coordinates.y = e.layerY;// - puzzleCanvas.offsetTop;
    }
    else if (e.offsetX || e.offsetX == 0) {
        coordinates.x = e.offsetX - puzzleCanvas.offsetLeft;
        coordinates.y = e.offsetY - puzzleCanvas.offsetTop;
    }
    else {
        var touch = e.changedTouches[0];
        coordinates.x = touch.pageX - puzzleCanvas.offsetLeft;
        coordinates.y = touch.pageY - puzzleCanvas.offsetTop;
    }
    selectedPiece = checkPieceClicked();
    if (selectedPiece != null) {
        drawingSurface.clearRect(selectedPiece.xPos, selectedPiece.yPos, pieceWidth, pieceHeight);
        drawingSurface.save();
        drawingSurface.globalAlpha = .9;
        drawingSurface.drawImage(originalImage,
             selectedPiece.sx * xScaling, selectedPiece.sy * yScaling,
              pieceWidth * xScaling,
               pieceHeight * yScaling,
                coordinates.x - (pieceWidth / 2), coordinates.y - (pieceHeight / 2),
                 pieceWidth, pieceHeight);
        drawingSurface.restore();
        if(!touchMeDaddy){
            document.onmousemove = updatePuzzle;
            document.onmouseup = pieceDropped;
        }
        else{
            document.ontouchmove = updatePuzzle;
            document.ontouchend = pieceDropped;
        }
    }
}

function checkPieceClicked() {
    var piece;
    for (var i = 0; i < pieces.length; i++) {
        piece = pieces[i];
        if (coordinates.x < piece.xPos 
            || coordinates.x > (piece.xPos + pieceWidth) 
            || coordinates.y < piece.yPos 
            || coordinates.y > (piece.yPos + pieceHeight)) {
            //PIECE NOT HIT
        }
        else {
            return piece;
        }
    }
    return null;
}

function updatePuzzle(e) {
    pieceToDrop = null;
    if (e.layerX || e.layerX == 0) {
        coordinates.x = e.layerX;
        coordinates.y = e.layerY;
    }
    else if (e.offsetX || e.offsetX == 0) {
        coordinates.x = e.offsetX - puzzleCanvas.offsetLeft;
        coordinates.y = e.offsetY - puzzleCanvas.offsetTop;
    }
    else {
        var touch = e.changedTouches[0];
        coordinates.x = touch.pageX - puzzleCanvas.offsetLeft;
        coordinates.y = touch.pageY - puzzleCanvas.offsetTop;
    }
    drawingSurface.clearRect(0, 0, puzzleWidth, puzzleHeight);
    var piece;
    for (var i = 0; i < pieces.length; i++) {
        piece = pieces[i];
        if (piece == selectedPiece) {
            continue;
        }
        drawingSurface.drawImage(
            originalImage, 
            piece.sx * xScaling, piece.sy * yScaling, 
            pieceWidth * xScaling, pieceHeight * yScaling,
             piece.xPos, piece.yPos,
              pieceWidth, pieceHeight);
        drawingSurface.strokeRect(piece.xPos, piece.yPos, pieceWidth, pieceHeight);
        if (pieceToDrop == null) {
            if (coordinates.x < piece.xPos || coordinates.x > (piece.xPos + pieceWidth) || coordinates.y < piece.yPos || coordinates.y > (piece.yPos + pieceHeight)) {
                //NOT OVER
            }
            else {
                pieceToDrop = piece;
                drawingSurface.save();
                drawingSurface.globalAlpha = .4;
                drawingSurface.fillStyle = puzzleHoverColor;
                drawingSurface.fillRect(pieceToDrop.xPos, pieceToDrop.yPos, pieceWidth, pieceHeight);
                drawingSurface.restore();
            }
        }
    }
    drawingSurface.save();
    drawingSurface.globalAlpha = .6;
    drawingSurface.drawImage(
        originalImage, 
        selectedPiece.sx * xScaling, selectedPiece.sy * yScaling,
         pieceWidth * xScaling,
          pieceHeight * yScaling,
           coordinates.x - (pieceWidth / 2), coordinates.y - (pieceHeight / 2),
            pieceWidth, pieceHeight);
    drawingSurface.restore();
    drawingSurface.strokeRect(coordinates.x - (pieceWidth / 2), coordinates.y - (pieceHeight / 2), pieceWidth, pieceHeight);
}

function pieceDropped(e) {
    if(!touchMeDaddy){
        document.onmousemove = null;
        document.ontouchmove = null;
    }
    else{
        document.onmouseup = null;
        document.ontouchend = null;
    }
    if (pieceToDrop != null) {
        var tmp = { xPos: selectedPiece.xPos, yPos: selectedPiece.yPos };
        selectedPiece.xPos = pieceToDrop.xPos;
        selectedPiece.yPos = pieceToDrop.yPos;
        pieceToDrop.xPos = tmp.xPos;
        pieceToDrop.yPos = tmp.yPos;
    }
    resetPuzzleAndCheckWin();
}

function resetPuzzleAndCheckWin() {
    drawingSurface.clearRect(0, 0, puzzleWidth, puzzleHeight);
    var gameWin = true;
    var piece;
    for (var i = 0; i < pieces.length; i++) {
        piece = pieces[i];
        drawingSurface.drawImage(
            originalImage,
             piece.sx * xScaling, piece.sy * yScaling,
              pieceWidth * xScaling, pieceHeight * yScaling,
               piece.xPos, piece.yPos,
                pieceWidth, pieceHeight);
        drawingSurface.strokeRect(piece.xPos, piece.yPos, pieceWidth, pieceHeight);
        if (piece.xPos != piece.sx || piece.yPos != piece.sy) {
            gameWin = false;
        }
    }
    if (gameWin) {
        setTimeout(gameOver, 1000);
    }
}

function gameOver() {
    document.onmousedown = null;
    document.ontouchstart = null;
    document.onmousemove = null;
    document.ontouchmove = null;
    document.onmouseup = null;
    document.ontouchend = null;
    initPuzzle();
    createTitle("Tap me");
    if(!touchMeDaddy){
        document.onmousedown = continueToDetails;
    }
    else{
        document.ontouchstart = continueToDetails;
        document.ontouchend = continueToDetails;
    }
}

function continueToDetails(){
    add_to_score(currentWorth, SHIP_TYPE);
    window.location.href = "details.html?type=" + SHIP_TYPE +"&imageLink=" + exactLink;
}

$(document).ready(init);